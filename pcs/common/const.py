from typing import NewType

from pcs.common.tools import Version

PcmkRoleType = NewType("PcmkRoleType", str)

PCMK_ROLE_STARTED = PcmkRoleType("Started")
PCMK_ROLE_STOPPED = PcmkRoleType("Stopped")
PCMK_ROLE_PROMOTED = PcmkRoleType("Promoted")
PCMK_ROLE_UNPROMOTED = PcmkRoleType("Unpromoted")
PCMK_ROLE_PROMOTED_LEGACY = PcmkRoleType("Master")
PCMK_ROLE_UNPROMOTED_LEGACY = PcmkRoleType("Slave")
PCMK_ROLE_PROMOTED_PRIMARY = PCMK_ROLE_PROMOTED_LEGACY
PCMK_ROLE_UNPROMOTED_PRIMARY = PCMK_ROLE_UNPROMOTED_LEGACY
PCMK_ROLES_PROMOTED = (PCMK_ROLE_PROMOTED, PCMK_ROLE_PROMOTED_LEGACY)
PCMK_ROLES_UNPROMOTED = (PCMK_ROLE_UNPROMOTED, PCMK_ROLE_UNPROMOTED_LEGACY)
PCMK_ROLES_RUNNING = (
    (PCMK_ROLE_STARTED,) + PCMK_ROLES_PROMOTED + PCMK_ROLES_UNPROMOTED
)
PCMK_ROLES = (PCMK_ROLE_STOPPED,) + PCMK_ROLES_RUNNING
PCMK_NEW_ROLES_CIB_VERSION = Version(3, 7, 0)
PCMK_RULES_NODE_ATTR_EXPR_WITH_INT_TYPE_CIB_VERSION = Version(3, 5, 0)
PCMK_ON_FAIL_DEMOTE_CIB_VERSION = Version(3, 4, 0)
